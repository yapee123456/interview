import java.math.BigDecimal;

/**
 * author: zkq 2022/02/17
 * 商品
 */
public class Goods {
    //商品名称
    private String goodsName;

    //商品的单价
    private float price;


    public Goods(String goodsName, float price) {
        if(goodsName==null || goodsName.trim().length()==0 || price<= 0){
            throw new RuntimeException("商品名不能为空，且价格要大于0");
        }
        this.goodsName = goodsName;
        this.price = price;
    }

    public String getGoodsName() {
        return goodsName;
    }

    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }
}
