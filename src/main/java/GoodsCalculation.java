import java.math.BigDecimal;

/**
 * author: zkq 2022/02/17
 * 商品计算类
 */
public class GoodsCalculation implements Calculation{


    /**
     *
     * @param price 商品价格
     * @param weight 商品重量
     * @param discount 商品折扣
     * @param accuracy 小数点后保留的精度
     * @return
     */
    @Override
    public BigDecimal calculation(float price, float weight, float discount, int accuracy) {
        if(price<=0){
            throw new RuntimeException("商品价格不能小于0");
        }

        if(weight<=0){
            throw new RuntimeException("商品重量不能小于0");
        }

        if(discount<=0 || discount>1){
            throw new RuntimeException("商品折扣不能小于0或者大于1");
        }

        if(accuracy<0){
            throw new RuntimeException("精度值不能小于0");
        }

        BigDecimal bigDecimal1 = new BigDecimal(String.valueOf(price));
        BigDecimal bigDecimal2 = new BigDecimal(String.valueOf(weight));
        BigDecimal bigDecimal3 = new BigDecimal(String.valueOf(discount));

        return ((bigDecimal1.multiply(bigDecimal2)).multiply(bigDecimal3)).setScale(accuracy,BigDecimal.ROUND_HALF_UP);
    }

    /**
     * @param totalAmount    商品总价
     * @param reduceAmount   可减免金额
     * @param standardAmount 达到减免金额的标准
     * @return
     */
    @Override
    public BigDecimal aiscountActivity(BigDecimal totalAmount, float reduceAmount, float standardAmount) {

        if(totalAmount.floatValue()<=0){
            throw new RuntimeException("商品总价不能小于等于0");
        }

        if(reduceAmount<0){
            throw new RuntimeException("减免金额不能小于0");
        }

        if(standardAmount<0){
            throw new RuntimeException("标准金额不能小于0");
        }

        if(totalAmount.floatValue() >= standardAmount ){
            BigDecimal bigDecimal1 = new BigDecimal(String.valueOf(reduceAmount));
            return totalAmount.subtract(bigDecimal1);
        }else {
            return  totalAmount;
        }
    }
}
