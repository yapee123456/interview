import java.math.BigDecimal;

/**
 * author: zkq 2022/02/17
 * 程序入口
 * 面试题存放于document文件夹下
 */
public class Main {
    public static void main(String[] args) {

        //初始化商品信息
        Goods apple = new Goods("苹果", 8.0F);
        Goods strawberry = new Goods("草莓", 13.0F);
        Goods mango = new Goods("芒果", 20.0F);

        //初始化商品计算类
        Calculation calculation = new GoodsCalculation();

        //初始化计算结果的精度
        int accuracy = 2;

        //题一：题1，A买若干斤苹果和草莓需要多少钱
        //假设买2斤苹果，2斤草莓，一共需要的金额为
        //初始化题一的折扣率
        float ch01Discount = 1.0F;
        BigDecimal ch01 = (calculation.calculation(apple.getPrice(),2.0F,ch01Discount,accuracy))
                .add(calculation.calculation(strawberry.getPrice(),2.0F,ch01Discount,accuracy));
        System.out.println("题1：2斤苹果和2斤草莓一共需要："+ch01);

        //题二：B买了若干苹果，草莓和芒果
        //初始化题二折扣率
        float ch02Discount = 1.0F;
        //假设买2斤苹果，2斤草莓，2斤芒果，一共需要的金额为
        BigDecimal ch02 = ((calculation.calculation(apple.getPrice(),2.0F,ch02Discount,accuracy))
                .add(calculation.calculation(strawberry.getPrice(),2.0F,ch02Discount,accuracy)))
                .add(calculation.calculation(mango.getPrice(),2.0F,ch02Discount,accuracy));
        System.out.println("题2：2斤苹果,2斤草莓和2斤芒果一共需要："+ch02);

        //题三：C买的草莓8折优惠
        //初始化题二折扣率
        //草莓8折，其他原价
        float ch03Discounta = 1.0F;
        float ch03Discountb = 0.8F;
        //假设买2斤苹果，2斤草莓，2斤芒果，一共需要的金额为
        BigDecimal ch03 = ((calculation.calculation(apple.getPrice(),2.0F,ch03Discounta,accuracy))
                .add(calculation.calculation(strawberry.getPrice(),2.0F,ch03Discountb,accuracy)))
                .add(calculation.calculation(mango.getPrice(),2.0F,ch03Discounta,accuracy));
        System.out.println("题3：2斤苹果,2斤草莓和两斤芒果一共需要："+ch03);

        //题四：D买满100有减10的活动
        //初始化题二折扣率
        float ch04Discount = 1.0F;
        //初始化满减活动的参数
        float reduceAmount  = 10.0F;
        float standardAmount = 100.0F;

        //假设买2斤苹果，2斤草莓，3斤芒果，一共需要的金额为
        BigDecimal ch04 = ((calculation.calculation(apple.getPrice(),2.0F,ch04Discount,accuracy))
                .add(calculation.calculation(strawberry.getPrice(),2.0F,ch04Discount,accuracy)))
                .add(calculation.calculation(mango.getPrice(),3.0F,ch04Discount,accuracy));
        System.out.println("题4：2斤苹果,2斤草莓和3斤芒果一共需要："+calculation.aiscountActivity(ch04,reduceAmount,standardAmount));
    }
}
