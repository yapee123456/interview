import java.math.BigDecimal;

/**
 * author: zkq 2022/02/17
 * 计算接口
 */
public interface Calculation {
    /**
     *
     * @param price 价格
     * @param weight 重量
     * @param discount 折扣
     * @param accuracy 小数点后保留的精度
     * @return
     */
    public BigDecimal calculation(float price,float weight,float discount,int accuracy);


    /**
     *
     * @param totalAmount  商品总价
     * @param reduceAmount 可减免金额
     * @param standardAmount 达到减免金额的标准
     * @return
     */
    public BigDecimal aiscountActivity(BigDecimal totalAmount,float reduceAmount, float standardAmount);
}
